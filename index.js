//  Концепція прототипів, яка використовується в JavaScript, проста. Якщо об'єкт B є прототипом об'єкта A, то всякий раз, коли у B є властивість,
//  наприклад колір, A  успадкує той же самий колір, якщо інше не вказано явно. І нам не потрібно повторювати всю інформацію про A,
//  яку він успадковує від  B. A  може успадковуватися від B і B, в свою чергу, може успадковуватися від  C  і т.д.
//  Це називається  ланцюжком прототипів. Наслідування працює на всій протяжності ланцюжка. Наприклад,
//  якщо  C  вказує колір, але ні B ні A колір не вказують, то він успадкується від C.

//  У Java існує ключове слово super, яке позначає суперклас, тобто клас, похідним від якого є поточний клас.
// Ключове слово super можна використовувати для виклику конструктора суперкласу і для звернення до члену суперкласу, захованого членом підкласу.
//  Виклик методу super () завжди повинен бути першим оператором, виконуваним всередині конструктора підкласу.



class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get lang() {
        return this._lang;
    }

    set lang(value) {
        this._lang = value;
    }

    get salary() {
        return this._salary * 3;
    }
}

const programmer1 = new Programmer("Bill", 76, 12000, ["fortran", "cobol"]);
const programmer2 = new Programmer("Jon", 22, 2000, ["java", "c#"]);
const programmer3 = new Programmer("Anders", 67, 8000, ["pascal", "basic"]);

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);
